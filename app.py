import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go

import json

import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

app = dash.Dash(__name__)

# ---------- Import and clean data (importing csv into pandas)
# df = pd.read_csv("intro_bees.csv")

with open('C:/Users/seanh/OneDrive/Desktop/Uni-Informatik/SS21/Datenbanksysteme/Projekt/Database/total.json') as f:
    data = json.load(f)

df = pd.DataFrame(data)

country_names = df['country_name'].unique()
years = df['year_total'].unique()

country_options = [{"label": country, "value": country} for country in country_names]

data_options = [
    {"label": "gdp_value", "value": "gdp_value"} ,
    {"label": "population_value", "value": "population_value"},
    {"label": "co2_value", "value": "co2_value"},
    {"label": "agriculture_value", "value": "agriculture_value"},
    {"label": "livestock_value", "value": "livestock_value"}
    ]

#df = pd.read_json(r'C:\Users\seanh\OneDrive\Desktop\Uni-Informatik\SS21\Datenbanksysteme\Projekt\Database\total.json', lines=True)

#df = df.groupby(['State', 'ANSI', 'Affected by', 'Year', 'state_code'])[['Pct of Colonies Impacted']].mean()
#df.reset_index(inplace=True)
#print(df[:5])

# ------------------------------------------------------------------------------
# App layout
app.layout = html.Div([

    html.H1("DBS Projekt 2021", style={'text-align': 'center'}),
    html.H2("by Sabina Puchin, Sean Hacker, Leslie Hoferichter", style={'text-align': 'center'} ),

    dcc.Dropdown(id="slct_country_name",
                options=
                    country_options
                    ,
                multi=False,
                value="Germany",
                style={'width': "40%"}
                ),
    html.Br(),
    html.Br(),    
    dcc.Dropdown(id="slct_first",
                options=
                    data_options
                    ,
                multi=False,
                value="population_value",
                style={'width': "40%"}
                ),    
    html.Br(),
    html.Br(),
    dcc.Dropdown(id="slct_second",
                options=
                    data_options
                    ,
                multi=False,
                value="gdp_value",
                style={'width': "40%"}
                ),
    html.Br(),
    html.Br(),
    html.Div(id='output_container1', children=[]),    
    html.Br(),
    dcc.Graph(id='my_map1', figure={}),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Br(),
    html.Div(id='output_container2', children=[]),    
    html.Br(),
    dcc.Graph(id='my_map2', figure={})

 ])


# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
@app.callback(
    [Output(component_id='output_container2', component_property='children'),
     Output(component_id='my_map1', component_property='figure')],
    [Input(component_id='slct_country_name', component_property='value'),
    Input(component_id='slct_first', component_property='value')]
)
def update_graph(country_slctd, data_first ):
       
    container = ""
    dff = df.copy()
    dff = dff[dff["country_name"] == country_slctd]

    fig = go.Figure(
        data=[
            go.Bar(
                name='population total', x=years, y=dff[data_first] 
            )
        ]
    )
    fig.update_layout(
        yaxis = dict(
            tickmode = 'array',
            tickvals = [],
            ticktext = []
        ),
        title = dict(text=data_first)
    )
    
    return container, fig


@app.callback(
    [Output(component_id='output_container1', component_property='children'),
     Output(component_id='my_map2', component_property='figure')],
    [Input(component_id='slct_country_name', component_property='value'),
    Input(component_id='slct_second', component_property='value')]
)
def update_graph(country_slctd, select_second):
    container = ""      
    dff = df.copy()
    dff = dff[dff["country_name"] == country_slctd]

     #Plotly Graph Objects (GO)
    fig = go.Figure(
            data=[
                go.Bar(
                    name='gdp value', x=years, y=dff[select_second] 
                )
            ]
        )
    fig.update_layout(
        yaxis = dict(
            tickmode = 'array',
            tickvals = [],
            ticktext = []
        ),
        title = dict(text=select_second)
    )
    
    
    return container, fig


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    app.run_server(debug=True)


